package advent.day1;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Day1 {
    public static void main(String[] args) throws IOException {
        String input = new String(new FileInputStream("day1.txt").readAllBytes());
        System.out.println(Arrays.stream(input.split("\n")).map(Day1::replaceString).map(c->Arrays.stream(c.split("")).
                filter(x->x.toCharArray()[0] >= '0' && x.toCharArray()[0] <= '9').collect(Collectors.toList())).map(x->Integer.parseInt(x.get(0)+x.get(x.size()-1))).reduce((a,b)->a+b).get());
    }

    public static String replaceString(String string){
        Map<String,Integer> didgetmap = new HashMap<>();
        String[] s = new String[]{string};
        didgetmap.put("one",1);
        didgetmap.put("two",2);
        didgetmap.put("three",3);
        didgetmap.put("four",4);
        didgetmap.put("five",5);
        didgetmap.put("six",6);
        didgetmap.put("seven",7);
        didgetmap.put("eight",8);
        didgetmap.put("nine",9);
        didgetmap.entrySet().stream().forEach(x->

        {

            if(s[0].contains(x.getKey())) {
                s[0] = s[0].replaceAll(x.getKey(), x.getKey()+x.getValue().toString()+x.getKey());
            }

        });

        return s[0];
    }
}
